<?php

require_once DIR_SYSTEM . 'library/code/code_menvio/vendor/autoload.php';
require_once DIR_SYSTEM . 'library/code/vendor/autoload.php';

use Code\MelhorEnvio\MelhorEnvio;

/**
 * Class ControllerExtensionShippingCodeMelhorenvio
 *
 * © Copyright 2013-2021 Codemarket - Todos os direitos reservados.
 *
 * @property \DB\MySQLi db
 * @property \Request request
 * @property \Response response
 * @property \Loader load
 */
class ControllerExtensionShippingCodeMelhorenvio extends Controller
{
    private $conf;

    public function __construct($registry)
    {
        parent::__construct($registry);

        if (empty($this->request->get['route'])) {
            die('route not found');
        }

        try {
            $this->load->model('module/codemarket_module');
        } catch (\Exception $e) {
            die('Model não instalado');
        }

        $this->conf = $this->model_module_codemarket_module->getModulo('524');

        if ($this->request->get['route'] == 'extension/shipping/code_melhorenvio/get_all') {
            \CodeLibrary\Request::get();
        } else if ($this->request->get['route'] != 'extension/shipping/code_melhorenvio/get_quotes' && !isset($this->session->data['user_id'])) {
            die('unauthorized');
        }
    }

    public function get_quote()
    {
        header('Content-Type: application/json');

        $order_id = !empty($this->request->get['order_id']) ? $this->request->get['order_id'] : null;

        if (!$order_id) {
            return json_encode(['success' => false]);
        }

        $query = $this->db->query('SELECT * FROM ' . DB_PREFIX . 'code_melhorenvio WHERE order_id = ' . (int) $order_id);

        if ($query->row) {
            $data = $query->row;

            return array_merge($data['data'], ['etiqueta' => $query->row['etiqueta']]);
        }

        return json_encode(['success' => false]);
    }

    public function order()
    {
        $lib = new MelhorEnvio($this->registry);

        $this->response->addHeader('Content-Type: application/json');

        $order_id = !empty($this->request->get['id']) ? $this->request->get['id'] : null;

        if (!$order_id) {
            return $this->response->setOutput(json_encode(['success' => false]));
        }

        $query = $this->db->query('SELECT * FROM ' . DB_PREFIX . 'code_melhorenvio WHERE order_id = ' . (int) $order_id);

        if ($query->row) {
            $data = $query->row;

            $query = $this->db->query('SELECT * FROM ' . DB_PREFIX . "order_product WHERE order_id = '" . (int) $order_id . "'");
            $products = $query->rows;

            //print_r($products);
            $total = 0;
            $insuranceValue = 0;
            $insuranceValueProduct = 0;

            $productsK = [];
            foreach ($products as $k => $product) {
                $total += $product['quantity'];

                //Adicionando o nome da Opção ao nome do Produto
                $query = $this->db->query('SELECT * FROM ' . DB_PREFIX . "order_option WHERE order_id = '" . (int) $order_id . "' AND order_product_id = '" . (int) $product['order_product_id'] . "'");
                $orderOption = $query->row;
                if (!empty($orderOption['name']) && $orderOption['value']) {
                    $product['name'] .= ' - ' . $orderOption['name'] . ': ' . $orderOption['value'];
                }

                $productsK[$product['order_product_id']] = [
                    'id'       => $product['product_id'],
                    'name'     => html_entity_decode($product['name']),
                    'model'    => $product['model'],
                    'price'    => (float) $product['price'],
                    'quantity' => (int) $product['quantity'],
                ];

                //DECLARAR VALOR
                $totalPrice = $product['price'] * $product['quantity'];

                if (!empty($this->conf->declarar_tabela2) && !empty($product['product_id'])) {
                    $declararGet = $this->db->query("SELECT * FROM " . $this->conf->declarar_tabela2 . " 
                        WHERE 
                        product_id = '" . (int) $product['product_id'] . "' 
                        LIMIT 1
                    ");
                }

                if (!empty($this->conf->declarar_campo2) && !empty($declararGet->row[$this->conf->declarar_campo2])) {
                    $insuranceValueProduct = ((float) $declararGet->row[$this->conf->declarar_campo2] * $product['quantity']);
                } else {
                    $insuranceValueProduct = (float) $totalPrice;
                }

                //Caso o preço no carrinho for menor que o custo, usar ele como custo
                if ($totalPrice < $insuranceValue) {
                    $insuranceValueProduct = (float) $totalPrice;
                }

                $insuranceValue += $insuranceValueProduct;
            }
            //print_r($productsK); exit();

            $packages = empty($data['packages']) ? [] : json_decode($data['packages']);

            foreach ($packages as $package) {
                if (!empty($package->cart->id)) {
                    $isOnCart = $lib->checkCart($package);
                    $tracking = $lib->trackPackage($package);

                    if (!$isOnCart || empty($tracking) || $tracking->status === 'canceled') {
                        $lib->removeCartFromPackage($order_id, $package);
                    }

                    $package->isOnCart = $isOnCart;
                    $package->tracking = $tracking;
                } else {
                    $package->isOnCart = false;
                    $package->tracking = false;
                }
            }

            //print_r(json_decode($data['data']));

            return $this->response->setOutput(json_encode([
                'quote'          => json_decode($data['data']),
                'packages'       => $packages,
                'conf'           => $lib->conf,
                'products'       => $productsK,
                'totalProducts'  => $total,
                'insuranceValue' => $insuranceValue,
            ]));
        }

        return $this->response->setOutput(json_encode(['success' => false]));
    }

    public function get_quotes()
    {
        $this->response->addHeader('Content-type: application/json');

        try {
            $query = $this->db->query('SELECT * FROM ' . DB_PREFIX . 'code_melhorenvio ORDER BY date_created DESC LIMIT 30');

            if ($query->rows) {
                $results = [];
                foreach ($query->rows as $i => $row) {
                    if (empty($row['data'])) {
                        continue;
                    }

                    $results[$i] = json_decode($row['data'], true);
                    $results[$i]['order'] = $row['order_id'];
                    $results[$i]['status'] = $row['status'];
                    $results[$i]['date'] = date('d/m/Y H:i:s', strtotime($row['date_created']));
                }

                $this->response->setOutput(json_encode([
                    'success' => true,
                    'results' => $results,
                ]));

                return true;
            }
        } catch (\Exception $e) {
            $this->response->setOutput(json_encode([
                'success' => false,
                'error'   => $e->getMessage(),
            ]));

            return true;
        }

        $this->response->setOutput(json_encode([
            'success' => false,
            'results' => null,
        ]));

        return true;
    }

    public function get_all()
    {
        \CodeLibrary\Request::get();

        $this->response->addHeader('Content-type: application/json');

        $limit = !empty($this->request->get['limit']) ? $this->request->get['limit'] : 30;
        $status = !empty($this->request->get['status']) ? $this->request->get['status'] : '';

        try {
            if (!empty($this->request->get['status'])) {
                $sql = 'SELECT * FROM ' . DB_PREFIX . "code_melhorenvio
                WHERE status = '" . $this->db->escape($status) . "'
                ORDER BY date_created DESC LIMIT  " . ($limit) . ' ';
            } else {
                $sql = 'SELECT * FROM ' . DB_PREFIX . 'code_melhorenvio
                ORDER BY date_created DESC LIMIT  ' . ($limit) . ' ';
            }

            $query = $this->db->query($sql);

            if ($query->rows) {
                $results = [];
                foreach ($query->rows as $i => $row) {
                    $results[$i]['data'] = json_decode($row['data'], true);
                    $results[$i]['packages'] = json_decode($row['packages'], true);
                    $results[$i]['status'] = $row['status'];
                    $results[$i]['order'] = $row['order_id'];
                    $results[$i]['date'] = date('d/m/Y H:i:s', strtotime($row['date_created']));
                }

                $this->response->setOutput(json_encode([
                    'success' => true,
                    'results' => $results,
                ]));

                return true;
            }
        } catch (\Exception $e) {
            $this->response->setOutput(json_encode([
                'success' => false,
                'error'   => $e->getMessage(),
            ]));

            return true;
        }

        $this->response->setOutput(json_encode([
            'success' => false,
            'results' => null,
        ]));

        return true;
    }

    public function record()
    {
        $etiqueta = $this->request->post['etiqueta'] === 'null' ? null : $this->request->post['etiqueta'];
        $order = (int) $this->request->post['orderId'] === 'null' ? null : $this->request->post['orderId'];

        if ($order) {
            $this->db->query('UPDATE ' . DB_PREFIX . "code_melhorenvio SET etiqueta = '" . $etiqueta . "' WHERE order_id = " . $order);
        } else {
            $this->db->query('UPDATE ' . DB_PREFIX . "code_melhorenvio SET etiqueta = '" . $etiqueta . "' WHERE etiqueta = " . $etiqueta);
        }
    }

    public function add()
    {
        $lib = new MelhorEnvio($this->registry);

        $this->response->addHeader('Content-Type: application/json');

        $post = json_decode(file_get_contents('php://input'));

        foreach ($post->packages as &$package) {
            if (isset($package)) {
                unset($package->currentProductSelection);
            }
            if (isset($package)) {
                unset($package->newProducts);
            }
        }

        $order = (int) $post->order;

        if ($order) {
            foreach ($post->packages as &$package) {
                if (empty($package->isOnCart) && $post->buy == true) {
                    $cart = $lib->comparEtiqueta($order, $package);

                    if (!empty($cart->error)) {
                        if (!empty($cart->message)) {
                            $cart->error = $cart->message . ' ' . $cart->error;
                        }

                        return $this->response->setOutput(json_encode(['success' => false, 'error' => $cart->error]));
                    }

                    if (!empty($cart->errors)) {
                        if (!empty($cart->message)) {
                            $cart->errors = $cart->message . ' ' . print_r($cart->errors, true);
                        } else {
                            $cart->errors = print_r($cart->errors, true);
                        }

                        return $this->response->setOutput(json_encode(['success' => false, 'error' => $cart->errors]));
                    }

                    $package->cart = $cart;
                }
            }

            $packages = json_encode($post->packages, JSON_PRETTY_PRINT);

            $this->db->query('UPDATE ' . DB_PREFIX . "code_melhorenvio SET packages = '" . $this->db->escape($packages) . "' WHERE order_id = " . $order);

            return $this->response->setOutput(json_encode(['success' => true]));
        }

        return $this->response->setOutput(json_encode(['success' => false]));
    }

    public function remove()
    {
        $lib = new MelhorEnvio($this->registry);

        $this->response->addHeader('Content-Type: application/json');

        $post = json_decode(file_get_contents('php://input'));

        foreach ($post->packages as &$package) {
            if (isset($package)) {
                unset($package->currentProductSelection);
            }
            if (isset($package)) {
                unset($package->newProducts);
            }
        }

        // remove from melhor envio
        if ($post->package->isOnCart == true) {
            $lib->removeFromCart($post->package);
        }

        $order = (int) $post->order;

        if ($order) {
            $packages = json_encode($post->packages, JSON_PRETTY_PRINT);

            $this->db->query('UPDATE ' . DB_PREFIX . "code_melhorenvio SET packages = '" . $this->db->escape($packages) . "' WHERE order_id = " . $order);

            return $this->response->setOutput(json_encode(['success' => true]));
        }

        return $this->response->setOutput(json_encode(['success' => false]));
    }

    public function config()
    {
        $menvio = new MelhorEnvio($this->registry);

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($menvio->conf));
    }
    /*
    public function test()
    {
        $carts = [
            [
                'cart' => [
                    'id' => 'teste',
                ],
            ],
            [
                'cart' => [
                    'id' => 'teste 2',
                ],
            ],
        ];

        ChromePhp::log(MelhorEnvio::findIndex($carts, 'cart.id', 'teste 2'));
    }

    public function testb()
    {
        $me = new MelhorEnvio($this->registry);

        $me->autoPost(1812);
    }
    */
}
